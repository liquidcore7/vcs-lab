export default interface IMoney {
    readonly amount: number;
    readonly currencyCode: string;
}
