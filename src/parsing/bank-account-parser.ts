import IBalanceParser from "./balance-parser";
import IMoney from "../money";


export interface IBankAccount {
    readonly ibanNumber: string;
    readonly currencyCode: string;
    readonly currentBalance: number;
    readonly holderName: string;
    readonly bankId: string;
    readonly beneficiaryBank: string;
}


export const BankAccountParser: IBalanceParser<IBankAccount> = {
    availableBalance: (on: IBankAccount): IMoney => ({
        amount: on.currentBalance,
        currencyCode: on.currencyCode
    })
}
