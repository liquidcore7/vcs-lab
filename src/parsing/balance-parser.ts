import IMoney from "../money";


export default interface IBalanceParser<EntityT> {
    readonly availableBalance: (on: EntityT) => IMoney;
}
