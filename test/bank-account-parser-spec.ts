import { expect } from 'chai';
import IMoney from "../src/money";
import {BankAccountParser, IBankAccount} from "../src/parsing/bank-account-parser";


describe('Bank account parser', () => {
    it('parsing a bank account object', () => {
        const expected: IMoney = {
            amount: 12500.0,
            currencyCode: 'GBP'
        };
        const bankAccount: IBankAccount = {
            ibanNumber: 'GB94BARC10201530093459',
            currencyCode: expected.currencyCode,
            currentBalance: expected.amount,
            holderName: 'NULP Alumni',
            bankId: 'HSBC',
            beneficiaryBank: 'HSBC'
        }
        const actual: IMoney = BankAccountParser.availableBalance(bankAccount);

        expect(actual).to.eql(expected);
    });
});
